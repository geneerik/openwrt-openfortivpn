# openwrt-openfortivpn
OpenWRT package for [openfortivpn: A Fortinet (and Ruijie) compatible client for PPP+SSL VPN tunnel services](https://github.com/adrienverge/openfortivpn)

## Build
Example for ar71xx and trunk.
```
curl -fSsL https://downloads.openwrt.org/releases/19.07.0-rc1/targets/mvebu/cortexa53/openwrt-sdk-19.07.0-rc1-mvebu-cortexa53_gcc-7.4.0_musl.Linux-x86_64.tar.xz -o openwrt-sdk.tar.xz
tar xf openwrt-sdk.tar.xz
cd openwrt-sdk-*/package
git clone https://gitlab.com/geneerik/openwrt-openfortivpn openfortivpn
cd ..
./scripts/feeds update base
./scripts/feeds install libopenssl resolveip ppp
make package/openfortivpn/compile V=s
```
