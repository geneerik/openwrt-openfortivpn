#
# Dockerfile for openwrt:mvebu
#
ARG IMAGE_NAME=registry.gitlab.com/openwrt/docker
ARG OPENWRT_TARGET=mvebu
ARG OPENWRT_SUBTARGET=cortexa53
ARG OPENWRT_TARGET_DASH_SUBSTARGET=${OPENWRT_TARGET}-${OPENWRT_SUBTARGET}
ARG OPENWRT_VERSION=19.07.0-rc1
ARG OPENWRT_VERSION_NAME=openwrt-${OPENWRT_VERSION}
#ARG IMAGE_TAG=${OPENWRT_TARGET_DASH_SUBSTARGET}-${OPENWRT_VERSION_NAME}
ARG IMAGE_TAG=base
FROM ${IMAGE_NAME}:${IMAGE_TAG}
MAINTAINER GeneErik

# reconnect args
ARG OPENWRT_TARGET=mvebu
ARG OPENWRT_SUBTARGET=cortexa53
ARG OPENWRT_TARGET_DASH_SUBSTARGET=${OPENWRT_TARGET}-${OPENWRT_SUBTARGET}
ARG OPENWRT_VERSION=19.07.0-rc1
ARG OPENWRT_VERSION_NAME=openwrt-${OPENWRT_VERSION}

ARG OPENWRT_FILE_HOST=downloads.openwrt.org
ARG OPENWRT_TARGET_SLASH_SUBSTARGET=${OPENWRT_TARGET}/${OPENWRT_SUBTARGET}
ARG OPENWRT_DOWNLOAD_PATH=releases/${OPENWRT_VERSION}/targets/${OPENWRT_TARGET_SLASH_SUBSTARGET}
ARG OPENWRT_URL=https://${OPENWRT_FILE_HOST}/${OPENWRT_DOWNLOAD_PATH}
ARG OPENWRT_IMAGE_FILE_NAME
#ARG OPENWRT_IMAGE_FILE_NAME="openwrt-imagebuilder.*x86_64.tar.xz"
ARG OPENWRT_IMAGE_DOWNLOAD_FILE
ARG OPENWRT_SDK_FILE_NAME
ARG OPENWRT_SDK_DOWNLOAD_FILE
#ARG OPENWRT_SDK_DOWNLOAD_FILE="openwrt-sdk-.*.Linux-x86_64.tar.xz"
#ENV OPENWRT_IMG=$(curl ${OPENWRT_URL} | sed -ne "s/.*>\\(openwrt-imagebuilder-[^<]*\\).*\\.tar.xz/\\1/p")
#ARG OPENWRT_IMG=openwrt-imagebuilder-${OPENWRT_VERSION}-mvebu-cortexa53.Linux-x86_64
#ENV OPENWRT_IMG=${OPENWRT_IMG}
#ENV OPENWRT_SDK=$(curl ${OPENWRT_URL} | sed -ne "s/.*>\\(openwrt-sdk-[^<]*\\).*/\\1/p")
#ARG OPENWRT_SDK=openwrt-sdk-${OPENWRT_VERSION}-mvebu-cortexa53_gcc-7.4.0_musl.Linux-x86_64
#ENV OPENWRT_SDK=${OPENWRT_SDK}
#ENV OPENWRT_IMG_URL=${OPENWRT_URL}/${OPENWRT_IMG}.tar.xz
#ENV OPENWRT_SDK_URL=${OPENWRT_URL}/${OPENWRT_SDK}.tar.xz
ENV OPENWRT_TARGET=${OPENWRT_TARGET}
ENV OPENWRT_SUBTARGET=${OPENWRT_SUBTARGET}
ENV OPENWRT_TARGET_DASH_SUBSTARGET=${OPENWRT_TARGET_DASH_SUBSTARGET}
ENV OPENWRT_SOC_TARGET=${OPENWRT_TARGET_SLASH_SUBSTARGET}
ENV OPENWRT_VERSION_NAME=${OPENWRT_VERSION_NAME}
ENV OPENWRT_VERSION=${OPENWRT_VERSION_NAME}

ENV OPENWRT_BUILD_USER=build
WORKDIR /home/${OPENWRT_BUILD_USER}

# Make sure keys are in there
RUN curl -fL https://gitlab.com/openwrt/docker/-/raw/master/docker-common.sh -O && \
    chmod +x docker-common.sh && \
    ./docker-common.sh

# get the build hash file if hashes not supplied
ARG OPENWRT_HASH_DOWNLOAD_FILE=${OPENWRT_URL}/sha256sums
ARG OPENWRT_HASH_DOWNLOAD_FILE_CONTENTS
RUN \
    if [ -z "${OPENWRT_HASH_DOWNLOAD_FILE_CONTENTS}" ]; then \
        ( \
            set -ex; \
            export GNUPGHOME="${GNUPGHOME:-/keys/gpg/}"; \
            export USIGNHOME="${USIGNHOME:-/keys/usign/}"; \
            curl "${OPENWRT_HASH_DOWNLOAD_FILE}" -fL -o sha256sums && \
            (curl "${OPENWRT_HASH_DOWNLOAD_FILE}.asc" -fL -o sha256sums.asc || true) && \
            (curl "${OPENWRT_HASH_DOWNLOAD_FILE}/sha256sums.sig" -fL -o sha256sums.sig || true) && \
            ([ ! -f sha256sums.asc ] || gpg --with-fingerprint --verify sha256sums.asc sha256sums) && \
            (if [ -f sha256sums.sig ]; then \
                if hash signify-openbsd 2>/dev/null; then \
                    # debian
                    SIGNIFY_BIN=signify-openbsd; \
                else \
                    # alpine
                    SIGNIFY_BIN=signify; \
                fi; \
                VERIFIED=; \
                for KEY in "$USIGNHOME"* ; do \
                    echo "Trying $KEY..." && \
                    if "$SIGNIFY_BIN" -V -q -p "$KEY" -x sha256sums.sig -m sha256sums; then \
                        echo "...verified" && \
                        VERIFIED=1 && \
                        break; \
                    fi; \
                done; \
                if [ -z "$VERIFIED" ]; then \
                    echo "Could not verify using signature" && \
                    false; \
                fi; \
            fi) \
        ); \
    else \
        # If content was passed in, base64 decode it
        (printf "${OPENWRT_HASH_DOWNLOAD_FILE_CONTENTS}" | base64 -d > sha256sums); \
    fi

# get the image
#ARG OPENWRT_IMG=openwrt-imagebuilder-${OPENWRT_VERSION}-mvebu-cortexa53.Linux-x86_64
RUN set -xe; \
    export "OPENWRT_IMAGE_FILE_NAME=${OPENWRT_IMAGE_FILE_NAME:-openwrt-imagebuilder.*x86_64.tar.xz}"; \
    export FILE_NAME=$(grep "${OPENWRT_IMAGE_FILE_NAME}" sha256sums | cut -d "*" -f 2); \
    export OPENWRT_IMAGE_DOWNLOAD_FILE=${OPENWRT_URL}/${FILE_NAME}; \
    curl -fL "${OPENWRT_IMAGE_DOWNLOAD_FILE}" -o "${FILE_NAME}" && \
    # shrink checksum file to single desired file and verify downloaded archive
    (grep "$FILE_NAME" sha256sums > sha256sums_min) && \
    sha256sum -c sha256sums_min && \
    rm -f sha256sums_min && \
    # unpack the image and link it
    tar -xJf "${FILE_NAME}" && \
    rm "${FILE_NAME}" && \
    export FILE_NAME_BASE=$(basename -s .tar.xz ${FILE_NAME}); \
    ln -s "${FILE_NAME_BASE}" img
    # TODO: is there more to prep fo kmod building?

USER root
RUN pwd \
    && ls -la \
    && chmod 777 /tmp \
    && mkdir -p /data \
    && chown "${OPENWRT_BUILD_USER}:${OPENWRT_BUILD_USER}" /data
USER ${OPENWRT_BUILD_USER}

# TODO: add diversion for supplied sdk archive

# get the sdk
#ARG OPENWRT_SDK=openwrt-sdk-${OPENWRT_VERSION}-mvebu-cortexa53_gcc-7.4.0_musl.Linux-x86_64
RUN set -xe; \
    export "OPENWRT_SDK_FILE_NAME=${OPENWRT_IMAGE_FILE_NAME:-openwrt-sdk-.*.Linux-x86_64.tar.xz}"; \
    export FILE_NAME=$(grep "${OPENWRT_SDK_FILE_NAME}" sha256sums | cut -d "*" -f 2); \
    export OPENWRT_SDK_DOWNLOAD_FILE=${OPENWRT_URL}/${FILE_NAME}; \
    # TODO: add mechanism to divert for gli.net sdk download and unzip process (download.sh w target from https://github.com/gl-inet/sdk)
    curl -fL "${OPENWRT_SDK_DOWNLOAD_FILE}" -o "${FILE_NAME}" && \
    # shrink checksum file to single desired file and verify downloaded archive
    (grep "$FILE_NAME" sha256sums > sha256sums_min) && \
    sha256sum -c sha256sums_min && \
    rm -f sha256sums_min && \
    # unpack the sdk and link it
    tar -xJf "${FILE_NAME}" && \
    rm "${FILE_NAME}" && \
    export FILE_NAME_BASE=$(basename -s .tar.xz ${FILE_NAME}); \
    # TODO: linking is different for gli.net; eg. ln -s "sdk/1907/mvebu" sdk_dir
    ln -s "${FILE_NAME_BASE}" sdk_dir && \
    cd sdk_dir && \
    echo "Using SDK from revision $(sed -ne 's/REVISION:=//p' ./include/version.mk)" && \
    (cd staging_dir/toolchain-*/lib && \
    rm lib && \
    ln -s ../lib) && \
    rm -f staging_dir/host/lib/lib && \
    ln -s ../lib staging_dir/host/lib/lib && \
    ln -s /data bin && \
    ls -la
    # && ./scripts/feeds update -a

# Patch to enable gcc 10 if not supported
RUN \
    if grep 'gcc-10 --version' sdk_dir/include/prereq-build.mk; then \
        echo 'gcc 10 already supported'; \
    else \
        echo 'injecting support for gcc 10' && \
        sed -i "s/^\\(\\([\t ]*\\)gcc9\\( --version .*\\)\\)\$/\\1\n\\2gcc-10\\3/" sdk_dir/include/prereq-build.mk && \
        grep 'gcc-10 --version' sdk_dir/include/prereq-build.mk; \
    fi

# Patch to enable g++ 10 if not supported
RUN \
    if grep 'g++-10 --version' sdk_dir/include/prereq-build.mk; then \
        echo 'g++ 10 already supported'; \
    else \
        echo 'injecting support for g++ 10' && \
        sed -i "s/^\\(\\([\t ]*\\)g++9\\( --version .*\\)\\)\$/\\1\n\\2g++-10\\3/" sdk_dir/include/prereq-build.mk && \
        grep 'g++-10 --version' sdk_dir/include/prereq-build.mk; \
    fi

ENV SDK_DIR=/home/${OPENWRT_BUILD_USER}/sdk_dir
ENV STAGING_DIR=${SDK_DIR}/staging_dir

# TODO: ensure path set correctly to include staging bin dirs

LABEL org.opencontainers.image.authors='Gene Erik <geneerik@fossdevops.com>'
LABEL org.opencontainers.image.url='https://gitlab.com/geneerik/openwrt-openfortivpn'
LABEL org.opencontainers.image.documentation='https://gitlab.com/geneerik/openwrt-openfortivpn/-/blob/master/README.md'
LABEL org.opencontainers.image.source='https://gitlab.com/geneerik/openwrt-openfortivpn.git'
LABEL org.opencontainers.image.vendor='Gene Erik'
#LABEL org.opencontainers.image.licenses=''
LABEL org.opencontainers.image.title='registry.gitlab.com/geneerik/openwrt-openfortivpn'
LABEL org.opencontainers.image.description='Docker image for building openfortivpn for openwrt'
